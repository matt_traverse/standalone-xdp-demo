# Standalone XDP router demo

This is a standalone demo project for XDP-accelerated routing.

The original code came from the xdp-tutorial project:

https://github.com/xdp-project/xdp-tutorial/blob/7c515b64f38b7209f9bc620c581d016779b000ae/packet-solutions/xdp_prog_kern_03.c#L232

which is closely related to this kernel sample program:

https://github.com/torvalds/linux/blob/master/samples/bpf/xdp_fwd_kern.c

The Makefiles originate from the VyOS project which was using the
same XDP program for it's experimental XDP feature.
(They have since moved to VPP)

Source:
https://github.com/vyos/vyos-1x/tree/71f91f08f45c1167743d55b2168a4d45c11c8eae/src/xdp

## How to use

It is expected that you have `xdp-tools` installed and that
the interfaces are setup correctly for routing.

(Don't forget `sysctl net.ipv4.ip_forward=1`!)

```
xdp-loader load -s xdp_router eth1 xdp_router.o
xdp-loader load -s xdp_router eth2 xdp_router.o
```
